#include "SceneNode.hpp"
#include "Category.hpp"
#include <cassert>

SceneNode::SceneNode():
	mChildren(),
	mParent(nullptr)
{
}

SceneNode::~SceneNode()
{
}

//Add child to the node
void SceneNode::attachChild(Ptr child)
{
	child->mParent = this;
	mChildren.push_back(std::move(child));
}

//Remove child of the node
SceneNode::Ptr SceneNode::detachChild(const SceneNode &node)
{
	auto found = std::find_if(mChildren.begin(), mChildren.end(), 
							  [&](Ptr &p) {return p.get() == &node; });

	assert(found != mChildren.end() && "SceneNode::detachChild cannot find the node");

	Ptr result = std::move(*found);
	result->mParent = nullptr;
	mChildren.erase(found);
	return result;
}

void SceneNode::update(sf::Time dt)
{
	updateCurrent(dt);
	updateChild(dt);
}

sf::Vector2f SceneNode::getWorldPosition() const
{
	return getWorldTransform() * sf::Vector2f();
}

sf::Transform SceneNode::getWorldTransform() const
{
	sf::Transform transform = sf::Transform::Identity; //optional - can be omitted

	for (const SceneNode *node = this; node != nullptr; node = node->mParent)
		transform = node->getTransform() * transform;

	return transform;
}

void SceneNode::onCommand(const Command & command, sf::Time dt)
{
	// Command current node, if category matches
	if (command.category & getCategory())
		command.action(*this, dt);

	// Command children
	for (Ptr &child : mChildren)
		child->onCommand(command, dt);
}

unsigned int SceneNode::getCategory() const
{
	return Category::Type::Scene;
}

void SceneNode::updateCurrent(sf::Time dt)
{
}

void SceneNode::updateChild(sf::Time dt)
{
	for (Ptr &child : mChildren)
		child->update(dt);
}

void SceneNode::draw(sf::RenderTarget & target, sf::RenderStates states) const
{
	//combine the current transform
	states.transform *= getTransform();

	drawCurrent(target, states);
	drawChild(target, states);
}

void SceneNode::drawCurrent(sf::RenderTarget & target, sf::RenderStates states) const
{
}

void SceneNode::drawChild(sf::RenderTarget & target, sf::RenderStates states) const
{
	for (const Ptr &child : mChildren)
		child->draw(target, states);
}
