#pragma once
#include <vector>
#include <SFML/Graphics/Sprite.hpp>
#include "ResourceHolder.hpp"
#include "SceneNode.hpp"

class Field : public SceneNode
{
public:
	Field(int width, int height, const TextureHolder &textures);
	~Field();

	virtual void drawCurrent(sf::RenderTarget &target, sf::RenderStates states) const;

	int getHeight() const;
	int getWidth() const;

private:
	virtual void updateCurrent(sf::Time dt);

private:
	int								mWidth;
	int								mHeight;
	sf::Sprite						mSprite;
	std::vector<std::vector<int>>	mLines;
};
