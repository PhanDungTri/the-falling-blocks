#include "CommandQueue.hpp"



CommandQueue::CommandQueue()
{
}


CommandQueue::~CommandQueue()
{
}

void CommandQueue::push(Command & command)
{
	mQueue.push(command);
}

Command CommandQueue::pop()
{
	Command command = mQueue.front();
	mQueue.pop();
	return command;
}

bool CommandQueue::isEmpty() const
{
	return mQueue.empty();
}
