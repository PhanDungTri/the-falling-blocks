#include <stdexcept>
#include <cstdlib>
#include <ctime>
#include <iostream>
#include "Game.hpp"

int main(int argc, char *argv[])
{
	srand(time(nullptr));
	try
	{
		Game &game = Game::instance();
		game.run();
	}
	catch (std::exception& e)
	{
		std::cout << "\nEXCEPTION: " << e.what() << std::endl;
	}
}