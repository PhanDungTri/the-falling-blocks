#pragma once
#include <SFML/System/Time.hpp>
#include <SFML/Graphics/RenderWindow.hpp>
#include "World.hpp"
#include "Player.hpp"

class Game
{
public:
	static Game &instance();
	
	void run();

private:
	void processEvents();
	void update(sf::Time elapsedTime);
	void render();

private:
	sf::RenderWindow	mWindow;
	World				mWorld;
	Player				mPlayer;

/*Singleton*/
private:
	Game();
	~Game() = default;

	Game(const Game&)				= delete;
	Game &operator=(const Game&)	= delete;
	Game(Game&&)					= delete;
	Game &operator=(Game&&)			= delete;

private:
	static const sf::Time TimePerFrame;
};

